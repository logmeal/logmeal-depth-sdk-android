package es.logmeal.sdk;

import android.content.Context;
import android.util.Log;

import com.google.ar.core.Frame;
import com.google.ar.core.exceptions.NotYetAvailableException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import es.logmeal.sdk.exceptions.IncompatibleRgbImageType;

public class LogMealRecorder {

    private static final String TAG = LogMealRecorder.class.getSimpleName();

    private static final double ANGLE_START = 50;
    private static final double ANGLE_END = 130;
    private static final int CAPTURED_FRAMES_MIN = 30;
    private static final int CAPTURED_FRAMES_MAX = 50;
    private static final int MAX_IMG_SIZE = 640;

    private AnglesAnalysis anglesAnalysis = new AnglesAnalysis(ANGLE_START, ANGLE_END, CAPTURED_FRAMES_MAX, new float[]{0, 0, 0});

    // Files management
    private String outputPath;

    // Process flags
    private boolean isSuccessful = false;
    private boolean isFullCapture = false;
    private boolean started = false;
    private boolean isRecording = false;
    private final boolean applyKalman = true;
    private int frameCounter = 0;


    // Other variables for capture management
    private ArrayList<FrameData> framesList = new ArrayList<>();
    private SceneCenter sceneCenter;
    private KalmanFilter kalmanFilter;
    private LogMealDepthData logMealDepthData;
    private final int orientation;
    private final String tempDir;
    private boolean generateDebugFile = false;

    public LogMealRecorder(Context ctx) {
        this.orientation = ctx.getResources().getConfiguration().orientation;
        this.tempDir = ctx.getCacheDir().getAbsolutePath();
    }

    public LogMealRecorder(Context ctx, boolean generateDebugFile) {
        this.generateDebugFile = generateDebugFile;
        this.orientation = ctx.getResources().getConfiguration().orientation;
        this.tempDir = ctx.getCacheDir().getAbsolutePath();
    }

    public void processFrame(Frame frame) {
        if (!isRecording) {
            return;
        }

        try {
            if (outputPath == null) {
                generateOutFolder(String.valueOf(System.currentTimeMillis()));
            }

            // Store current camera parameters in an object
            FrameData frameData = new FrameData(frame, MAX_IMG_SIZE, orientation);

            // Initialize capture with first camera if not initialized yet
            if (!started) {
                initializeCapture(frameData.getCameraPose());
            }

            // Apply kalman to camera or simply return same camera pose
            if (applyKalman) {
                float[] updated_poseMatrix_1D = kalmanFilter.applyKalman(frameData.getCameraPose());
                frameData.addUpdatedPoseMatrix(updated_poseMatrix_1D);

                // Finish if kalman detects change of trajectory
                if (kalmanFilter.isTrajectoryChange()) {
                    stopRecording();
                    return;
                }
            } else {
                frameData.addUpdatedPoseMatrix(frameData.getCameraPose());
            }

            // The current frame added to the list.
            framesList = anglesAnalysis.updateFullFramesList(frameData);
            frameCounter = frameCounter + 1;

            // Update scene center with new camera pose
            sceneCenter.update_scene_center_from_cams(framesList);

            // Get number of captured frames (max one per angle range)
            // Compute relative angles based on the center, first camera proj and all the camera poses.
            // Update those values in frames_list
            framesList = anglesAnalysis.selectFrames(sceneCenter.get_weighted_center());

            Log.d(TAG, String.format("Last angle %s", framesList.get(framesList.size() - 1).getAngle()));

            // Finish the process when all frames are ready. (started, isTrajectoryChanged)
            if ((anglesAnalysis.getCapturedFramesCount() >= (CAPTURED_FRAMES_MAX)) && (anglesAnalysis.getMainRgbIdx() != -1)) {
                isFullCapture = true;
                stopRecording();
            }
        } catch (IOException e) {
            Log.e(TAG, "FrameData.generateJpegImage --> IOException in ImageConvertionUtils.toJpegImageBytes", e);
        } catch (NotYetAvailableException e) {
            Log.e(TAG, "FrameData.generateJpegImage --> Image not available yet", e);
        } catch (IncompatibleRgbImageType e) {
            Log.e(TAG, "FrameData.generateJpegImage --> Incompatible RGB image", e);
        }
    }

    public void startRecording() {
        isFullCapture = false;
        isSuccessful = false;
        started = false;
        isRecording = true;
        logMealDepthData = null;
    }

    public void stopRecording() {
        isRecording = false;
        started = false;
        anglesAnalysis.resetFullFrameList();
        Log.d(TAG, String.format("userStopButton capturedFramesCount: %d - getMainRgbIdx: %d", anglesAnalysis.getCapturedFramesCount(), anglesAnalysis.getMainRgbIdx()));
        if ((anglesAnalysis.getMainRgbIdx() != -1) && (anglesAnalysis.getCapturedFramesCount() > CAPTURED_FRAMES_MIN)) {
            isSuccessful = true;
            logMealDepthData = new LogMealDepthData(outputPath, generateDebugFile);
            logMealDepthData.generate(framesList, anglesAnalysis.getMainRgbIdx());
        }
    }

    // Method to initialize all the needed variables.
    private void initializeCapture(float[] poseMatrix) {
        frameCounter = 0;

        // Process Flags
        started = true;

        // Frames management
        sceneCenter = new SceneCenter();
        framesList = new ArrayList<>();
        anglesAnalysis = new AnglesAnalysis(
                ANGLE_START,
                ANGLE_END,
                CAPTURED_FRAMES_MAX,
                new float[]{poseMatrix[12], poseMatrix[13], poseMatrix[14]}); // x, y, z

        // Kalman
        if (applyKalman) {
            kalmanFilter = new KalmanFilter(poseMatrix);
        }
    }

    private void generateOutFolder(String folderName) throws IOException {
        Path tempOutPath = Paths.get(tempDir, folderName);
        if (!Files.exists(tempOutPath)) {
            Files.createDirectory(tempOutPath);
        }
        outputPath = tempOutPath.toString();
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public boolean isFullCapture() {
        return isFullCapture;
    }

    public boolean isTrajectoryChanged() {
        return kalmanFilter != null && kalmanFilter.isTrajectoryChange();
    }

    public LogMealDepthData getLogMealDepthData() {
        return logMealDepthData;
    }

    public boolean isRecording() {
        return isRecording;
    }
}
