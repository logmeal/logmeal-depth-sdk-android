package es.logmeal.sdk.sample.helpers;

import java.io.File;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class DepthImageApiHelper {

    private static final String TAG = DepthImageApiHelper.class.getSimpleName();

    public static final String IMAGE_DEPTH_URL = "https://api.logmeal.es/v2/image/segmentation/complete/quantity";

    private String customUrl;

    public Call sendRequest(String authToken,
                            String imagePath,
                            String sequencePath,
                            String camFocalLength,
                            String camPrincipalPoint,
                            String mainRgbIdx,
                            String cameraPose,
                            String version) {
        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("image", "image.jpg", RequestBody.create(new File(imagePath), MediaType.parse("image/jpeg")))
                .addFormDataPart("sequence", "sequence.avi", RequestBody.create(new File(sequencePath), MediaType.parse("video/mpeg")))
                .addFormDataPart("cam_focal_length", camFocalLength)
                .addFormDataPart("cam_principal_point", camPrincipalPoint)
                .addFormDataPart("main_rgb_idx", mainRgbIdx)
                .addFormDataPart("camera_pose", cameraPose)
                .addFormDataPart("version", version)
                .build();

        Request request = new Request.Builder()
                .url(customUrl != null ? customUrl : IMAGE_DEPTH_URL)
                .addHeader("Authorization", "Bearer " + authToken)
                .addHeader("Accept", "application/json")
                .post(requestBody)
                .build();

        return client.newCall(request);
    }

    public String getCustomUrl() {
        return customUrl;
    }

    public void setCustomUrl(String customUrl) {
        this.customUrl = customUrl;
    }
}