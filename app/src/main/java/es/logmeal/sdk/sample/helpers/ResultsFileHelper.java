package es.logmeal.sdk.sample.helpers;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import es.logmeal.sdk.LogMealDepthData;

public class ResultsFileHelper {
    private static final String TAG = ResultsFileHelper.class.getSimpleName();

    public static void moveResults(ContentResolver cr, Uri outputDirectoryUri, LogMealDepthData lmData) throws IOException {
        if (outputDirectoryUri == null) {
            throw new IOException("out folder not selected");
        }

        // Create a subfolder inside the selected directory
        Uri subfolderUri = createSubfolder(cr, outputDirectoryUri, String.valueOf(System.currentTimeMillis()));
        if (subfolderUri == null) {
            throw new IOException("Can't create subfolder");
        }

        List<File> files = new ArrayList<>();
        files.add(new File(lmData.getMainRgbPath()));
        files.add(new File(lmData.getVideoPath()));
        if (lmData.getDebugFilePath() != null) {
            files.add(new File(lmData.getDebugFilePath()));
        }

        for (File file : files) {
            Uri uri = createFileInDirectory(cr, subfolderUri, file.getName());
            if (uri != null) {
                try (FileInputStream in = new FileInputStream(file);
                     OutputStream out = cr.openOutputStream(uri)) {
                    byte[] buffer = new byte[1024];
                    int bytesRead;
                    while ((bytesRead = in.read(buffer)) != -1) {
                        out.write(buffer, 0, bytesRead);
                    }
                }
            }
        }
    }

    static private Uri createSubfolder(ContentResolver contentResolver, Uri parentUri, String subfolderName) throws FileNotFoundException {
        Uri docUri = DocumentsContract.buildDocumentUriUsingTree(parentUri, DocumentsContract.getTreeDocumentId(parentUri));
        return DocumentsContract.createDocument(contentResolver, docUri, DocumentsContract.Document.MIME_TYPE_DIR, subfolderName);
    }

    static private Uri createFileInDirectory(ContentResolver contentResolver, Uri directoryUri, String fileName) throws FileNotFoundException {
        return DocumentsContract.createDocument(contentResolver, directoryUri, "application/octet-stream", fileName);
    }
}
