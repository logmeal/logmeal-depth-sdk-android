# LogMeal SDK
This is the Android SDK of LogMeal. It's main purpose is to aid in the generation of needed files to use Depth Endpoints.
It can be used as a low level SDK withing ArCore or as a camera widget that obtains the needed data.

## 1. How to add this library to your project
1. Download LogMealSDK-release-[VERSION].aar from releases
2. Copy the downloaded file inside `app/libs` folder (create it if doesn't exists).
3. In `build.gradle` add the AAR file as a dependency and also include it's dependencies
```groovy
    // AAR file
    implementation(files("libs/LogMealSDK-release-[VERSION].aar"))

    // Take this versions as a reference. We recommend fixing every dependency
    implementation("com.google.ar:core:1.+")
    implementation("com.google.code.gson:gson:2.+")
    implementation("com.arthenica:ffmpeg-kit-min:6.+")
    implementation("org.apache.commons:commons-math3:3.+")
    implementation("androidx.exifinterface:exifinterface:1.+")
    implementation("androidx.appcompat:appcompat:1.6.+")
```

## 2. Workflow
This SDK provides a capture mode that allows obtaining data that can be used for the API.
It starts a capture process and will inform you when it is finished through an event listener. Once finished, it generates all the needed data to call the API. Alternatively, the user can stop the capture at will which can produce a successful or incorrect capture. 

## 3. How to use
First notice not all devices are capable of recording depth data, so you should check if the running device has that capability. We provide a helper class to check for this and also triggers ArCore install if this service is missing:
```java
import es.logmeal.sdk.helpers.ArCoreUtils;
//...
boolean supported = ArCoreUtils.isDeviceSupported(/* Activity */ activity);
//...
```

After checking device compatibility, there are two modes to integrate the SDK to your project. The easiest one is with a widget that gives almost all the functionality needed to plug and play. This is intended for developers that don't know or use ArCore and want a simple implementation. The second alternative is for developers who are already using ArCore and want to add the needed functionality to generate files to use Depth Endpoints.

**NOTE**: The following sections describes JAVA integration. We are improving docs on Kotlin and Jetpack Compose integration. Meanwhile there is a zip in the latest release called `kotlin-sample.zip` that you can download to see how kotlin integration can be done.

### 3.1 Widget integration
Using `es.logmeal.sdk.widgets.DepthRecorderView` is the easiest way to integrate this SDK. This case is used in the example application on the `es.logmeal.sdk.sample.DepthWasteExampleActivity` class.

#### 3.1.1 Add widget to your view
First let's add the widget to your View. It should appear on UI editor inside `Project` widgets palette but in case you don't see it, you can add this to your source code.

```xml
    <es.logmeal.sdk.widgets.DepthRecorderView
        android:id="@+id/lmRecorder"
    />
```
This will add a custom implementation of `GLSurfaceView` to your view and then you can edit the attributes on the UI editor or inside the XML.

#### 3.1.2 Add widget attribute to your activity
After editing with, height and position of the widget, you have to add a reference of the component inside your activity, add it as a LifeCycle Observer and provide a callback for the auto stop event.
```java
//...

// class attribute
private DepthRecorderView depthRecorderView;

@Override
protected void onCreate(Bundle savedInstanceState) {
    // ...

    // get LogMeal widget
    depthRecorderView = findViewById(R.id.lmRecorder);
    // add it as a LifeCycle observer 
    getLifecycle().addObserver(depthRecorderView);
    // add auto stop event listener
    depthRecorderView.setAutoStopRecordingEventListener(this::processRecordingResult);
    
    //...
}

private void processRecordingResult() {
    // code on stop recording. More on this later
}
```

#### 3.1.3 Provide your user a way to start and stop capture process
Create UI buttons and add onClick event listeners. These you will use `startRecording` and `stopRecording`. In the following example we also add a call to onAutoStopRecordingEvent in order to process the capture on user's stop.
```java
public void startRecording(View view) {
    depthRecorderView.startRecording();
}

public void stopRecording(View view) {
    depthRecorderView.stopRecording();
    processRecordingResult();
}
```

#### 3.1.4 Process result and send to API 
Now go to section 4 which explains this topic.

### 3.2 ArCore existing app integration
This is a more advanced integration an it's assumed that the developer knows ArCore. This case is used in the example application on the `es.logmeal.sdk.sample.DepthImageExampleActivity` class.

Typically an ArCore application has an Activity implementing `GLSurfaceView.Renderer` the `onDrawFrame` method where developers perform AR tasks. The SDK is integrated into this flow to obtain the necessary data. It is assumed that you will implement a UI mechanism to let the user activate a recording mode so that the process can start.

In order to follow an example for this integration we will assume the following mechanism that triggers start and stop recording:

```java
public class ArCoreActivity extends AppCompatActivity implements GLSurfaceView.Renderer {
    //...
    
    // activity state attributes 
    private enum AppState {IDLE, RECORDING}
    private final AtomicReference<AppState> currentState = new AtomicReference<>(AppState.IDLE);

    // start method associated to onClick button event
    public void startRecording(View view) {
        currentState.set(AppState.RECORDING);
    }

    // stop method associated to onClick button event
    public void stopRecording(View view) {
        currentState.set(AppState.IDLE);
    }
    
    //...
}
```

#### 3.2.1 Add LogMealRecorder attribute and call it's start and stop methods 
Inside your activity add an attribute of `LogMealRecorder` class and call `startRecording` and `stopRecording` methods.

```java
// add activity attribute
private LogMealRecorder lmRecorder;

public void startRecording(View view) {
    currentState.set(AppState.RECORDING);
    // add startRecording call
    lmRecorder = new LogMealRecorder(/* context */ this);
    lmRecorder.startRecording();
}

private void stopRecording() {
    currentState.set(AppState.IDLE);
    // add stopRecording call
    lmRecorder.stopRecording();
    // call method to process recoding result
    onStopRecording();
}

public void onStopRecording() {
    // code on stop recording. More on this later
}
 ```

#### 3.2.2  Add record on `onDrawFrame`
Now inside the `onDrawFrame` call the `captureFrame` method of `LogMealRecorder` class and also check for it's still recording. Following our example the coe. 
```java  
 @Override
public void onDrawFrame(GL10 gl) {
    //...

    // check if user triggered recording
    if (currentState.get() == AppState.RECORDING) {
        // process current frame
        lmRecorder.processFrame(/* ArCore Session */ session, /* ArCore Frame */ frame);
        
        // check if recording stopped. This can be due to a successful capture or an error 
        if (!lmRecorder.isRecording()) {
            // change app recording state
            currentState.set(AppState.IDLE);
            // call method to process recoding result
            onStopRecording();
        }
    }
    
    //...
}
 ```

#### 3.2.3 Process result and send to API
Now go to section 4 which explains this topic.

## 4 Process recording result and send to API
This process is common to both types of integration.

### 4.1 Process recording result
In case of recording auto stop or an user's triggered stop, you should check the if the result was correct. There are two stages, first you should check if the recording process was successful with the `LogMealRecorder.isSuccessful()` method, then you should check if the needed files were correctly generated with `LogMealRecorder.getLogMealDepthData().isSuccessfulGeneration()` method. The file generation process is asynchronous, so it's recommended to check for the future before processing. The following code shows that example.

```java
private void processRecordingResult() {
    LogMealRecorder lmRecorder = depthRecorderView.getLogMealRecorder();
    // you already have lmRecorder as class attribute if you choose advanced integration  
    if (lmRecorder.isSuccessful()) {
        lmRecorder
                .getLogMealDepthData()
                .getFuture()
                .thenAccept(unused -> {
                    if (lmRecorder.getLogMealDepthData().isSuccessfulGeneration()) {
                        // SUCCESS
                        sendDataToApi(lmRecorder.getLogMealDepthData());   
                    }
                    else {
                        // show error to user                
                    }
                });
    } else {
        // show error to user
    }
}
```

### 4.2 Send data to API
Once you have `LogMealDepthData` with successful data generated you can get all the required data to the depth API endpoint. Here is an example:

```java
private sendDataToApi(LogMealDepthData data) {
    String endpointUrl = "DEPTH_API_ENDPOINT";
    String token = "YOUR_TOKEN";
    OkHttpClient client = new OkHttpClient();
    
    // prepare body
    RequestBody requestBody = new MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("image", "image.jpg", RequestBody.create(new File(data.getMainRgbPath()), MediaType.parse("image/jpeg")))
            .addFormDataPart("sequence", "sequence.avi", RequestBody.create(new File(data.getVideoPath()), MediaType.parse("video/mpeg")))
            .addFormDataPart("cam_focal_length", data.getCamFocalLength().toJson())
            .addFormDataPart("cam_principal_point", data.getCamPrincipalPoint().toJson())
            .addFormDataPart("main_rgb_idx", String.valueOf(data.getMainRgbIdx()))
            .addFormDataPart("camera_pose", data.getCameraPoses().toJson())
            .addFormDataPart("version", data.getVersion())
            .build();

    // prepare request
    Request request = new Request.Builder()
            .url(endpointUrl)
            .addHeader("Authorization", "Bearer " + token)
            .addHeader("Accept", "application/json")
            .post(requestBody)
            .build();
    
    // send and process result
    Call call = client.newCall(request);
    call.enqueue(new Callback() {
        @Override
        public void onResponse(@NonNull Call call, @NonNull Response response) {
            // process success. Don't forget check status code
        }

        @Override
        public void onFailure(@NonNull Call call, @NonNull IOException e) {
           // process failure
        }
    });
}
```

## 5. Important classes
This section quickly highlights SDK more important classes and it's methods.

### 5.1 DepthRecorderView
This class is a widget that shows an ArCore camera preview with all the logic needed to start and stop depth recording process. It's main methods are:

```java
// start recording process
public void startRecording();
// stops recording process
public void stopRecording();
// register an event listener for process auto stop
public void setAutoStopRecordingEventListener(DepthRecorderAutoStopListener listener);
```

### 5.2 LogMealRecorder
This class is in charge of starting, stopping, and processing the camera frames in order to obtain the needed data. It's main methods are:
```java
// Start recording process
public void startRecording();
// Stops recording process
public void stopRecording();
// Process current Frame for depth data
public void processFrame(Session session, Frame frame);
// Checks if recording is still happening
public boolean isRecording();
// Checks if recording was successful
public boolean isSuccessful();
// Checks if recording was full capture or stopped programmatically
public boolean isFullCapture();
```

### 5.3 LogMealDepthData
This class holds generated depth data when a recording process was successful. It's main methods are:
```java
// get the future that will complete (or fail) when all files are generated
public CompletableFuture<Void> getFuture();

// getters of the individual fields needed to call API
public String getMainRgbPath();
public String getVideoPath();
public CamPoint getCamFocalLength();
public CamPoint getCamPrincipalPoint();
public CameraPosesMap getCameraPoses();
public int getMainRgbIdx();
public String getVersion();
```

### 5.4 ArCoreUtils
It's purpose is to simplify and centralize details about device support. It has one static method that returns an instance of `ArCoreSupport`. If ARCore wasn't installed, it will automatically show a popup to install it.
```java 
    public static ArCoreSupport isDeviceSupported(Activity activity);
```

### 5.5 ArCoreSupport
It contains the support information for the device. It has three getters:
```java 
    public boolean isArCoreSupported(); // ARCore is supported in this device?
    public boolean isDepthSupported(); // ARCore's Depth API is supported in this device?
    public ArCoreApk.Availability getArCoreAvailability(); // ARCore's availability for this device. Checkout https://developers.google.com/ar/reference/java/com/google/ar/core/ArCoreApk.Availability
```


### 5.6 CameraPermissionHelper
It's a simply helper to ask for camera permission. 
```java
    // Check to see we have the necessary permissions for this app.
    public static boolean hasCameraPermission(Activity activity);
    // Check to see we have the necessary permissions for this app, and ask for them if we don't.
    public static void requestCameraPermission(Activity activity);
    // Launch Application Setting to grant permission.
    public static void launchPermissionSettings(Activity activity);
    // Check to see if we need to show the rationale for this permission.
    public static boolean shouldShowRequestPermissionRationale(Activity activity);
```

The helper is usually used inside activities:
```java
@Override
protected void onResume() {
    // ...

    // ARCore requires camera permission to operate.
    if (!CameraPermissionHelper.hasCameraPermission(this)) {
        CameraPermissionHelper.requestCameraPermission(this);
    }

    // ...
}

@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] results) {
    super.onRequestPermissionsResult(requestCode, permissions, results);
    if (!CameraPermissionHelper.hasCameraPermission(this)) {
        Toast.makeText(this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                .show();
        if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
            CameraPermissionHelper.launchPermissionSettings(this);
        }
        finish();
    }
}
```
