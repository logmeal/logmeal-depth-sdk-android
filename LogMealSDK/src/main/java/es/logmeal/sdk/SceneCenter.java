package es.logmeal.sdk;

import java.util.ArrayList;


public class SceneCenter {

    private Matrix matrix = new Matrix();
    private float totw;
    private float[] totp;
    private float[] totp_weighted;

    public SceneCenter(){
        totw = 0.0F;
        totp = new float[] {0.0F, 0.0F, 0.0F};
        totp_weighted = new float[] {0.0F, 0.0F, 0.0F};
    }

    public void update_scene_center_from_cams(ArrayList<FrameData> frames_list) {

        float[][] cameraPose_last = frames_list.get(frames_list.size() - 1).getCameraPoseUpdated2D();

        // Select the third and fourth cols (and first three rows)
        float[] lc_col_2 = matrix.flatten_array(matrix.get_subarray(cameraPose_last, 0, 3, 2, 3));
        float[] lc_col_3 = matrix.flatten_array(matrix.get_subarray(cameraPose_last, 0, 3, 3, 4));

        for (int cx = 0; cx < frames_list.size(); cx++) {

            float[] tc_col_2 = matrix.flatten_array(matrix.get_subarray(frames_list.get(cx).getCameraPoseUpdated2D(), 0, 3, 2, 3));
            float[] tc_col_3 = matrix.flatten_array(matrix.get_subarray(frames_list.get(cx).getCameraPoseUpdated2D(), 0, 3, 3, 4));

            // Closest_point_2_lines
            Object[] closest_point = closest_point_2_lines(lc_col_3, lc_col_2, tc_col_3, tc_col_2);
            float[] p = (float[]) closest_point[0];
            float w = (float) closest_point[1];

            if (w > 0.00001) {
                for (int n = 0; n < p.length; n++)
                    totp[n] += (p[n] * w);
                totw += w;
            }
        }

        // Update weighted center
        apply_weight_scene_center();
    }

    public void apply_weight_scene_center() {
        for (int i = 0; i < totp_weighted.length; i++)
            totp_weighted[i] = totp[i] / totw;
    }

    public Object[] closest_point_2_lines(float[] oa, float[] da, float[] ob, float[] db) {

        // returns point closest to both rays of form o+t*d
        da = matrix.normalize(da);
        db = matrix.normalize(db);
        float[] c = matrix.computeCrossProduct(da, db);
        float denom = matrix.norm(c) * matrix.norm(c);

        float[] t = new float[oa.length];
        for (int i = 0; i < oa.length; i++)
            t[i] = ob[i] - oa[i];

        double[][] ta_matrix = new double[][] {matrix.arr_float2double(t), matrix.arr_float2double(db), matrix.arr_float2double(c)};
        double[][] tb_matrix = new double[][] {matrix.arr_float2double(t), matrix.arr_float2double(da), matrix.arr_float2double(c)};
        double ta = matrix.computeDeterminant(ta_matrix) / (denom + 1e-10);
        double tb = matrix.computeDeterminant(tb_matrix) / (denom + 1e-10);
        if (ta > 0) ta = 0;
        if (tb > 0) tb = 0;

        float[] result = new float[oa.length];
        for (int i = 0; i < oa.length; i++)
            result[i] = (float) ((oa[i] + (ta * da[i]) + ob[i] + (tb * db[i])) * 0.5);
        return new Object[]{result, denom};
    }

    public float[] get_weighted_center(){
        return totp_weighted;
    }
}
