package es.logmeal.sdk.widgets;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;

import com.google.ar.core.Camera;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import es.logmeal.sdk.LogMealRecorder;
import es.logmeal.sdk.helpers.DisplayRotationHelper;
import es.logmeal.sdk.helpers.TrackingStateHelper;
import es.logmeal.sdk.rendering.BackgroundRenderer;

public class DepthRecorderView extends GLSurfaceView implements GLSurfaceView.Renderer, LifecycleEventObserver {
    private static final String TAG = DepthRecorderView.class.getSimpleName();

    private final Object frameInUseLock = new Object();
    private final BackgroundRenderer backgroundRenderer = new BackgroundRenderer();
    private TrackingStateHelper trackingStateHelper;
    private DisplayRotationHelper displayRotationHelper;
    private enum AppState {IDLE, RECORDING}
    private final AtomicReference<AppState> currentState = new AtomicReference<>(AppState.IDLE);
    private Session session;
    private LogMealRecorder lmRecorder;
    private DepthRecorderAutoStopListener autoStopListener;
    private boolean enableDebug = false;


    public DepthRecorderView(Context context) {
        super(context);
    }

    public DepthRecorderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setAutoStopRecordingEventListener(DepthRecorderAutoStopListener listener) {
        this.autoStopListener = listener;
    }

    public void startRecording() {
        lmRecorder = new LogMealRecorder(getContext(), enableDebug);
        lmRecorder.startRecording();
        currentState.set(AppState.RECORDING);
    }

    public void stopRecording() {
        if (lmRecorder != null) {
            lmRecorder.stopRecording();
            currentState.set(AppState.IDLE);
        }
    }

    //region Getters and setters
    public void setEnableDebug(boolean enableDebug) {
        this.enableDebug = enableDebug;
    }

    public LogMealRecorder getLogMealRecorder() {
        return lmRecorder;
    }
    //endregion

    //region GLSurfaceView.Renderer
    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (session == null) return;
        displayRotationHelper.updateSessionIfNeeded(session);

        try {
            session.setCameraTextureName(backgroundRenderer.getTextureId());
            Frame frame = session.update();
            Camera camera = frame.getCamera();

            backgroundRenderer.draw(frame);

            trackingStateHelper.updateKeepScreenOnFlag(camera.getTrackingState());
            if (camera.getTrackingState() != TrackingState.TRACKING) return;

            if (currentState.get() == AppState.RECORDING) {
                lmRecorder.processFrame(frame);
                if (!lmRecorder.isRecording()) {
                    // recording stopped, can be successful or not
                    currentState.set(AppState.IDLE);
                    if (autoStopListener != null) {
                        autoStopListener.onAutoStopRecording();
                    }
                }
            }
        } catch (CameraNotAvailableException e) {
            Log.e(TAG, "Camera not available", e);
        } catch (Throwable t) {
            Log.e(TAG, "Exception on the OpenGL thread", t);
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        try {
            backgroundRenderer.createOnGlThread(getContext());
        } catch (IOException e) {
            Log.e(TAG, "Failed to read an asset file", e);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        displayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }
    //endregion

    //region Lifecycle

    @Override
    public void onStateChanged(@NonNull LifecycleOwner lifecycleOwner, @NonNull Lifecycle.Event event) {
        switch (event) {
            case ON_CREATE:
                this.onCreate();
                break;
            case ON_RESUME:
                this.onResume();
                break;
            case ON_DESTROY:
                this.onDestroy();
                break;
            case ON_PAUSE:
                this.onPause();
                break;
        }
    }

    public void onCreate() {
        trackingStateHelper = new TrackingStateHelper((Activity) getContext());
        displayRotationHelper = new DisplayRotationHelper(getContext());

        setPreserveEGLContextOnPause(true);
        setEGLContextClientVersion(2);
        setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        setRenderer(this);
        setRenderMode(RENDERMODE_CONTINUOUSLY);
        setWillNotDraw(true);

        try {
            session = new Session(getContext());
            Config config = session.getConfig();
            config.setFocusMode(Config.FocusMode.AUTO);
            config.setDepthMode(Config.DepthMode.AUTOMATIC);
            session.configure(config);
        } catch (UnavailableDeviceNotCompatibleException | UnavailableSdkTooOldException |
                 UnavailableApkTooOldException | UnavailableArcoreNotInstalledException e) {
            Log.e(TAG, "Exception creating session", e);
        }
    }

    @Override
    public void onResume() {
        if (session != null) {
            try {
                synchronized (frameInUseLock) {
                    // Enable raw depth estimation and auto focus mode while ARCore is running.
                    Config config = session.getConfig();
                    config.setFocusMode(Config.FocusMode.AUTO);
                    config.setDepthMode(Config.DepthMode.AUTOMATIC);
                    session.configure(config);
                    // To allow focus we need to pause and resume session after resume
                    // https://github.com/google-ar/arcore-android-sdk/issues/1312
                    session.resume();
                    session.pause();
                    session.resume();
                }
            } catch (CameraNotAvailableException e) {
                session = null;
                return;
            }
        }
        super.onResume();
        displayRotationHelper.onResume();
    }

    public void onDestroy() {
        if (session != null) {
            // stop recording before pausing the session
            this.stopRecording();
            session.close();
            session = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (session != null) {
            // stop recording before pausing the session
            this.stopRecording();
            // Note that the order matters - see note in onResume().
            // GLSurfaceView is paused before pausing the ARCore session, to prevent onDrawFrame
            // () from
            // calling session.update() on a paused session.
            displayRotationHelper.onPause();
            super.onPause();
            session.pause();
        }
    }

    //endregion
}
