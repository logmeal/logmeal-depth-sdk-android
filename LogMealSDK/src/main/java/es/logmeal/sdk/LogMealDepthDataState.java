package es.logmeal.sdk;

public enum LogMealDepthDataState {
    IDLE,
    IS_GENERATING,
    DONE_GENERATING,
    ERROR
}
