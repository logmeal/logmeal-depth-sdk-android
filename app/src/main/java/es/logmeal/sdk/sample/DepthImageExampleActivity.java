package es.logmeal.sdk.sample;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.ar.core.Camera;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;

import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import es.logmeal.sdk.LogMealRecorder;
import es.logmeal.sdk.helpers.DisplayRotationHelper;
import es.logmeal.sdk.helpers.TrackingStateHelper;
import es.logmeal.sdk.rendering.BackgroundRenderer;
import es.logmeal.sdk.sample.helpers.DepthImageApiHelper;
import es.logmeal.sdk.sample.helpers.ResultsFileHelper;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * This is a simple example that shows how to use ARCore Raw Depth API. The application will display
 * a 3D point cloud and allow the user control the number of points based on depth confidence.
 */
public class DepthImageExampleActivity extends AppCompatActivity implements GLSurfaceView.Renderer {
    private static final String TAG = DepthImageExampleActivity.class.getSimpleName();

    private final Object frameInUseLock = new Object();
    private enum AppState {IDLE, RECORDING}
    private final AtomicReference<AppState> currentState = new AtomicReference<>(AppState.IDLE);
    private Session session;
    private DisplayRotationHelper displayRotationHelper;
    private final BackgroundRenderer backgroundRenderer = new BackgroundRenderer();
    private final TrackingStateHelper trackingStateHelper = new TrackingStateHelper(this);
    private LogMealRecorder lmRecorder;
    private String token;
    private Uri outputDirectoryUri;
    private GLSurfaceView surfaceView;
    private TextView resultTextView;
    private Button startRecordingButton;
    private Button stopRecordingButton;
    private Button sendRecordingButton;

    /********************************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras() != null ? getIntent().getExtras().getString("token") : null;
        outputDirectoryUri = getIntent().getExtras() != null ? Uri.parse(getIntent().getExtras().getString("outFolder")) : null;

        setContentView(R.layout.activity_depth_image_advanced);
        surfaceView = findViewById(R.id.surfaceView);
        displayRotationHelper = new DisplayRotationHelper(this);

        // Set up rendering.
        surfaceView.setPreserveEGLContextOnPause(true);
        surfaceView.setEGLContextClientVersion(2);
        surfaceView.setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        surfaceView.setRenderer(this);
        surfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        surfaceView.setWillNotDraw(false);

        startRecordingButton = findViewById(R.id.preconsumptionStartRecordingBtn);
        stopRecordingButton = findViewById(R.id.preconsumptionStopRecordingBtn);
        sendRecordingButton = findViewById(R.id.preconsumptionSendResultsBtn);
        resultTextView = findViewById(R.id.preconsumptionResultsTextView);
        resultTextView.setMovementMethod(new ScrollingMovementMethod());
        EditText apiUrlInput = findViewById(R.id.preconsumptionApiUrlTextInput);

        resultTextView.setOnLongClickListener(v -> {
            copyToClipboard();
            return true;
        });

        apiUrlInput.setText(DepthImageApiHelper.IMAGE_DEPTH_URL);

        if (outputDirectoryUri == null) {
            updateUIErrorStarting(new Exception("out folder not selected"));
        }

        startRecordingButton.setOnClickListener(view -> startRecording());
        stopRecordingButton.setOnClickListener(view -> stopRecording());
        sendRecordingButton.setOnClickListener(view -> sendRequest());
    }

    private void copyToClipboard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("LM Depth copied", resultTextView.getText().toString());
        clipboard.setPrimaryClip(clip);
        // Show a Toast message (optional)
        Toast.makeText(this, "Result copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            synchronized (frameInUseLock) {
                // Enable raw depth estimation and auto focus mode while ARCore is running.
                Config config = session.getConfig();
                config.setFocusMode(Config.FocusMode.AUTO);
                config.setDepthMode(Config.DepthMode.AUTOMATIC);
                session.configure(config);
                // To allow focus we need to pause and resume session after resume
                // https://github.com/google-ar/arcore-android-sdk/issues/1312
                session.resume();
                session.pause();
                session.resume();
            }
        } catch (CameraNotAvailableException e) {
            session = null;
            return;
        }
        surfaceView.onResume();
        displayRotationHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (session != null) {
            // stop recording before pausing the session
            this.stopRecording();
            // Note that the order matters - see note in onResume().
            // GLSurfaceView is paused before pausing the ARCore session, to prevent onDrawFrame
            // () from
            // calling session.update() on a paused session.
            displayRotationHelper.onPause();
            surfaceView.onPause();
            session.pause();
        }
    }

    @Override
    protected void onDestroy() {
        if (session != null) {
            // stop recording before pausing the session
            this.stopRecording();
            session.close();
            session = null;
        }
        super.onDestroy();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        // Prepare the rendering objects. This involves reading shaders, so may throw an
        // IOException.
        try {
            backgroundRenderer.createOnGlThread(/*context=*/ this);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read an asset file", e);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        displayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }

    /********************************************************************************/

    @Override
    public void onDrawFrame(GL10 gl) {

        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        // Do not render anything or call session methods until session is created.
        if (session == null) return;
        displayRotationHelper.updateSessionIfNeeded(session);

        try {
            // Acquire and display frame
            session.setCameraTextureName(backgroundRenderer.getTextureId());
            Frame frame = session.update();
            Camera camera = frame.getCamera();
            backgroundRenderer.draw(frame);
            trackingStateHelper.updateKeepScreenOnFlag(camera.getTrackingState());
            if (camera.getTrackingState() != TrackingState.TRACKING) return;

            // Acquire data if recording
            if (currentState.get() == AppState.RECORDING) {
                lmRecorder.processFrame(frame);
                if (!lmRecorder.isRecording()) {
                    // recording stopped, can be successful or not
                    currentState.set(AppState.IDLE);
                    updateUIAfterStopping();
                }
            }
        } catch (CameraNotAvailableException e) {
            Log.e(TAG, "FrameData.generateJpegImage --> Image not available yet", e);
        } catch (Throwable t) {
            Log.e(TAG, "Exception on the OpenGL thread", t);
        }
    }


    /********************************************************************************/

    // Send data Green Button
    public void sendRequest() {
        runOnUiThread(() -> {
            sendRecordingButton.setEnabled(false);
            sendRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            resultTextView.setText("Sending...");
        });

        TextInputEditText textInputEditText = findViewById(R.id.preconsumptionApiUrlTextInput);
        String customUrl = Objects.requireNonNull(textInputEditText.getText()).toString();

        String authToken = this.token != null ? this.token : "";

        DepthImageApiHelper apiSender = new DepthImageApiHelper();
        apiSender.setCustomUrl(customUrl);

        Call call = apiSender.sendRequest(authToken,
                lmRecorder.getLogMealDepthData().getMainRgbPath(),
                lmRecorder.getLogMealDepthData().getVideoPath(),
                lmRecorder.getLogMealDepthData().getCamFocalLength().toJson(),
                lmRecorder.getLogMealDepthData().getCamPrincipalPoint().toJson(),
                String.valueOf(lmRecorder.getLogMealDepthData().getMainRgbIdx()),
                lmRecorder.getLogMealDepthData().getCameraPoses().toJson(),
                lmRecorder.getLogMealDepthData().getVersion());

        call.enqueue(new Callback() {
            final String separator = "\r\n========================\r\n";

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                String resultText;
                if (response.isSuccessful()) {
                    int statusCode = response.code();
                    if (statusCode < 200 || statusCode >= 300) {
                        resultText = "ERROR: API respond with error " + separator + "Status Code: " + statusCode + separator + response.body().string();
                    } else {
                        resultText = "SUCCESS " + separator + "Status Code: " + statusCode + separator;
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            Log.d("Response JSON", jsonObject.toString());
                            resultText += "ImageID: " + jsonObject.optString("imageId", "-") + separator + jsonObject;
                        } catch (JSONException e) {
                            resultText += "Error parsing response";
                            Log.e(TAG, "Error parsing response", e);
                        }
                    }
                } else {
                    resultText = "ERROR: Could not send request " + separator + response.message() + separator + response.body().string();
                    Log.d(TAG, String.format("Error sending request: %s - %s", response.message(), response.body().string()));
                }

                String finalResultText = resultText;
                runOnUiThread(() -> {
                    resultTextView.setText(finalResultText);
                    sendRecordingButton.setEnabled(true);
                    sendRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                });
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "Error sending request", e);
                runOnUiThread(() -> {
                    resultTextView.setText("ERROR: Could not send request " + separator + e.getMessage());
                    sendRecordingButton.setEnabled(true);
                    sendRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                });
            }
        });
    }


    /********************************************************************************/

    private void startRecording() {
        lmRecorder = new LogMealRecorder(this, true);
        try {
            if (outputDirectoryUri == null) {
                throw new IOException("out folder not selected");
            }
            lmRecorder.startRecording();
            currentState.set(AppState.RECORDING);
            updateUIForCapturing();
        } catch (IOException e) {
            Log.e(TAG, "Error creating output directory", e);
            updateUIErrorStarting(e);
        }
    }

    private void stopRecording() {
        if (lmRecorder == null) return;
        lmRecorder.stopRecording();
        currentState.set(AppState.IDLE);
        updateUIAfterStopping();
    }

    private void updateUIErrorStarting(Throwable th) {
        String msg = "Error starting recording\n" + th.getMessage();
        runOnUiThread(() -> {
            resultTextView.setText(msg);
            startRecordingButton.setEnabled(true);
            startRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            sendRecordingButton.setEnabled(false);
            sendRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            stopRecordingButton.setEnabled(false);
            stopRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
        });
    }

    private void updateUIForCapturing() {
        runOnUiThread(() -> {
            resultTextView.setText("Capturing...");
            startRecordingButton.setEnabled(false);
            startRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            sendRecordingButton.setEnabled(false);
            sendRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            stopRecordingButton.setEnabled(true);
            stopRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        });
    }

    private void updateUIAfterStopping() {

        String baseMsg =
                (lmRecorder.isFullCapture() || lmRecorder.isTrajectoryChanged()
                        ? "AUTO-STOPPED capture. "
                        : "Capture stopped by user. ") +
                        (lmRecorder.isTrajectoryChanged() ? "\nCHANGED TRAJECTORY." : "");
        String lastLineMsg = lmRecorder.isSuccessful() ? "\nSUCCESS. Generating depth data..." : "\nERROR. Missing needed info to send to API.";

        String initialMsg = baseMsg + lastLineMsg;
        runOnUiThread(() -> {
            startRecordingButton.setEnabled(true);
            startRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            stopRecordingButton.setEnabled(false);
            stopRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));

            resultTextView.setText(initialMsg);

            // disable send button until depth data is generated
            sendRecordingButton.setEnabled(false);
            sendRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
        });

        if (lmRecorder.isSuccessful()) {
            lmRecorder.getLogMealDepthData()
                    .getFuture()
                    .thenAccept(unused -> {
                        if (lmRecorder.getLogMealDepthData().isSuccessfulGeneration()) {
                            try {
                                ResultsFileHelper.moveResults(getContentResolver(), outputDirectoryUri, lmRecorder.getLogMealDepthData());
                                String successMsg = baseMsg + "\nSUCCESS. Generated depth data. READY.";
                                runOnUiThread(() -> {
                                    resultTextView.setText(successMsg);
                                    sendRecordingButton.setEnabled(true);
                                    sendRecordingButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                                });
                            } catch (IOException e) {
                                Log.e(TAG, "Error moving files", e);
                                updateUIErrorStarting(e);
                            }
                        }
                    }).exceptionally(throwable -> {
                        Log.e(TAG, "Error generating depth data", throwable);
                        updateUIErrorStarting(throwable);
                        return null;
                    });
        }
    }
}
