package es.logmeal.sdk.exceptions;

public class IncompatibleRgbImageType extends Exception {
    public IncompatibleRgbImageType() {
        super("Incompatible RGB image type");
    }
}
