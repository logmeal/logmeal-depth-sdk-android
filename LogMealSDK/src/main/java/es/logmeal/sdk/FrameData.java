package es.logmeal.sdk;

import android.graphics.ImageFormat;
import android.media.Image;

import com.google.ar.core.Anchor;
import com.google.ar.core.Camera;
import com.google.ar.core.Frame;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.NotYetAvailableException;

import java.io.IOException;
import java.util.Arrays;

import es.logmeal.sdk.exceptions.IncompatibleRgbImageType;
import es.logmeal.sdk.helpers.ImageConvertionUtils;

public class FrameData {

    private int maxSize = 640;
    private int originalSize = -1;
    private int originalWidth = -1;
    private int originalHeight = -1;
    private float angle = -1;
    private int angleRangeIdx = -1;
    private byte[] byteImage;
    private final float[] focalLength;
    private final float[] principalPoint;
    private float[] cameraPose = new float[16];
    private float[] updatedCameraPose;
    private float[][] updatedCameraPose_2D;

    private Matrix matrix = new Matrix();

    private String imagePath;
    private int orientation;

    public FrameData(Frame frame, int newSize, int orientation) throws IncompatibleRgbImageType, IOException, NotYetAvailableException {
        this.maxSize = newSize;
        this.orientation = orientation;
        Camera camera = frame.getCamera();
        this.focalLength = camera.getImageIntrinsics().getFocalLength();
        this.principalPoint = camera.getImageIntrinsics().getPrincipalPoint();
        parseCameraPose(camera.getPose());
        parseFrameImage(frame);
    }

    public void addUpdatedPoseMatrix(float[] newCameraPose){
        updatedCameraPose = Arrays.copyOf(newCameraPose, 16);
        updatedCameraPose_2D = matrix.poseMatrix_1D_to_2D(updatedCameraPose);
    }

    private void parseCameraPose(Pose nativeCameraPose) {
        nativeCameraPose.toMatrix(cameraPose, 0);
    }

    private void parseFrameImage(Frame frame) throws NotYetAvailableException, IncompatibleRgbImageType, IOException {
        // Get image size
        Image image = frame.acquireCameraImage();
        originalWidth = image.getWidth();
        originalHeight = image.getHeight();

        originalSize = Math.max(originalWidth, originalHeight);

        int format = image.getFormat();
        if (format != ImageFormat.YUV_420_888) {
            image.close();
            throw new IncompatibleRgbImageType();
        }

        byteImage = ImageConvertionUtils.toJpegImageBytes(image, 100);
        image.close();
    }

    // Basic img data
    public float[] getFocalLength() {
        return focalLength;
    }
    public float[] getPrincipalPoint() {
        return principalPoint;
    }
    public float[] getCameraPose(){
        return cameraPose;
    }
    public float[] getCameraPoseUpdated(){return updatedCameraPose;}
    public float[][] getCameraPoseUpdated2D(){
        return updatedCameraPose_2D;
    }

    // Angle data
    public float getAngle(){return angle;}
    public int getAngleRangeIdx(){
        return angleRangeIdx;
    }
    public void setAngle(float newAngle){
        angle = newAngle;
    }
    public void setAngleRangeIdx(int newAngleRangeIdx){
        angleRangeIdx = newAngleRangeIdx;
    }


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public byte[] getByteImage() {
        return byteImage;
    }

    public int getOriginalSize() {
        return originalSize;
    }

    public int getOriginalWidth() {
        return originalWidth;
    }

    public int getOriginalHeight() {
        return originalHeight;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public int getOrientation() {
        return orientation;
    }
}
