package es.logmeal.sdk;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class Matrix {

    public float[][] transposeMatrix(float[][] matrix) {
        int rows = matrix.length;
        int cols = matrix[0].length;
        float[][] transposedMatrix = new float[cols][rows];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                transposedMatrix[j][i] = matrix[i][j];
        return transposedMatrix;
    }

    public float[][] reshape(float[] inputArray, int rows, int cols) {
        if (inputArray.length != rows*cols)
            throw new IllegalArgumentException("Input array has wrong size.");
        float[][] outputMatrix = new float[rows][cols];
        int index = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                outputMatrix[i][j] = (float) inputArray[index];
                index++;
            }
        }
        return outputMatrix;
    }

    public float[][] poseMatrix_1D_to_2D(float[] poseMatrix_1D) {
        return transposeMatrix(reshape(poseMatrix_1D, 4, 4));
    }

    public float[] subtract_vectors(float[] vec1, float[] vec2) {
        if (vec1.length != vec2.length)
            throw new IllegalArgumentException("Vectors should have same size");
        float[] subtraction = new float[vec1.length];
        for (int i = 0; i < vec1.length; i++) {
            subtraction[i] = vec1[i] - vec2[i];
        }
        return subtraction;
    }

    public float[] add_vectors(float[] vec1, float[] vec2) {
        if (vec1.length != vec2.length)
            throw new IllegalArgumentException("Vectors should have same size");
        float[] addition = new float[vec1.length];
        for (int i = 0; i < vec1.length; i++) {
            addition[i] = vec1[i] + vec2[i];
        }
        return addition;
    }

    public double[] arr_float2double(float[] array){
        double[] array_double = new double[array.length];
        for (int i = 0; i < array.length; i ++){
            array_double[i] = (double) array[i];
        }
        return array_double;
    }

    public float[] arr_double2float(double[] array){
        float[] array_double = new float[array.length];
        for (int i = 0; i < array.length; i ++){
            array_double[i] = (float) array[i];
        }
        return array_double;
    }

    // Computes the norm
    public float norm(float[] data) {
        float aux = 0;
        for (float datum : data) aux += (datum * datum);
        return (float) Math.sqrt(aux);
    }

    public float[][] get_subarray(float[][] array, int row_st, int row_end, int col_st, int col_end){
        int numRows = row_end - row_st;
        int numCols = col_end - col_st;
        float[][] subarray = new float[numRows][numCols];
        for (int r = row_st; r < row_end; r++)
        {
            for (int c = col_st; c < col_end; c++)
            {
                subarray[r-row_st][c-col_st] = array[r][c];
            }
        }
        return subarray;
    }

    public float[] normalize(float[] data) {
        float data_norm = norm(data);
        float[] aux = new float[data.length];
        for (int i = 0; i < data.length; i++)
            aux[i] = data[i] / data_norm;
        return aux;
    }

    public float[] flatten_array(float[][] array){
        int numRows = array.length;
        int numCols = array[0].length;
        float[] flat_arr = new float[numRows*numCols];
        for (int r = 0; r < numRows; r++)
        {
            for (int c = 0; c < numCols; c++)
            {
                flat_arr[(r*numCols) + c] = array[r][c];
            }
        }
        return flat_arr;
    }

    public float[] computeCrossProduct(float[] a, float[] b) {

        if (a.length != 3 || b.length != 3)
            throw new IllegalArgumentException("Both input arrays must have exactly 3 elements.");
        float[] result = new float[3];
        result[0] = a[1] * b[2] - a[2] * b[1];
        result[1] = a[2] * b[0] - a[0] * b[2];
        result[2] = a[0] * b[1] - a[1] * b[0];
        return result;
    }

    public double computeDeterminant(double[][] matrix) {
        RealMatrix realmatrix = MatrixUtils.createRealMatrix(matrix);
        return new org.apache.commons.math3.linear.LUDecomposition(realmatrix).getDeterminant();
    }

    public double dotProduct(float[] vector1, float[] vector2) {
        double result = 0.0;
        for (int i = 0; i < vector1.length; i++) {
            result += vector1[i] * vector2[i];
        }
        return result;
    }

}
