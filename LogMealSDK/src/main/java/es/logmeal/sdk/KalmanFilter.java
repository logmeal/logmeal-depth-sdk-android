/*
* This class contains all the resources needed to perform Kalman filter
* */
package es.logmeal.sdk;

import org.apache.commons.math3.filter.DefaultMeasurementModel;
import org.apache.commons.math3.filter.DefaultProcessModel;
import org.apache.commons.math3.filter.MeasurementModel;
import org.apache.commons.math3.filter.ProcessModel;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.Arrays;


public class KalmanFilter {
    private final Matrix matrix = new Matrix();
    private org.apache.commons.math3.filter.KalmanFilter f;
    private int  init_counter = 0;
    private float[]  shift = {0.0F, 0.0F, 0.0F};
    private float thr_max = 0.05f;
    private boolean trajectoryChange = false;

    public KalmanFilter(float[] poseMatrix){
        this.f = initialize_kalman(poseMatrix);
    }

    // It initializes the Kalman filter using default values.
    private org.apache.commons.math3.filter.KalmanFilter initialize_kalman(float[] poseMatrix) {
        trajectoryChange = false;
        init_counter = 0;

        RealMatrix A = new Array2DRowRealMatrix(new double[][]
                {{1, 1, 0.5, 0, 0, 0, 0, 0, 0},
                {0, 1, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 1, 0.5, 0, 0, 0},
                {0, 0, 0, 0, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 1, 0.5},
                {0, 0, 0, 0, 0, 0, 0, 1, 1},
                {0, 0, 0, 0, 0, 0, 0, 0, 1}});

        RealMatrix B = null;

        // Process Uncertainty Noise (Size X x X)
        RealMatrix Q = new Array2DRowRealMatrix(new double[][]{
                {25, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 10, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 25, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 10, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 25, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 10, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 1}});

        RealVector initialState = new ArrayRealVector(new double[]{poseMatrix[12], 0, 0, poseMatrix[13], 0, 0, poseMatrix[14], 0, 0});

        // Covariance Matrix is a identity matrix multiplied by 100000 (Size X x X)
        RealMatrix P = MatrixUtils.createRealIdentityMatrix(9);
        P = P.scalarMultiply(1000);

        // Measurament function (Size Z x X)
        RealMatrix H = new Array2DRowRealMatrix(new double[][]{
                {1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 1, 0, 0}});

        // Measurement Uncertainty Noise is a identity matrix multiplied by 25 (Size Z x Z)
        RealMatrix R = MatrixUtils.createRealIdentityMatrix(3);
        R = R.scalarMultiply(25);

        ProcessModel pm = new DefaultProcessModel(A, B, Q, initialState, P);
        MeasurementModel mm = new DefaultMeasurementModel(H, R);
        return new org.apache.commons.math3.filter.KalmanFilter(pm, mm);
    }

    public float[] applyKalman(float[] oldPoseMatrix) {
        // Given a camera pose, get the distance between the predicted and measured camera
        // 1. If distance is larger than threshold A, perform Kalman to correct position.
        // 2. If distance is larger than threshold B, trajectory has changed.
        //      2.1 If main frame and more than 30 frames has been captured -> successful.
        //      2.2 Else, discard.

        // Apply accumulated shift to xyz position
        float[] poseMatrix = Arrays.copyOf(oldPoseMatrix, oldPoseMatrix.length);

        poseMatrix = subtract_from_pose(poseMatrix, shift);

        // Get xyz position predicted by kalman
        float[] x =  get_prediction();

        // Get distance between measurement and prediction
        float[] distance_xyz = matrix.subtract_vectors(Arrays.copyOfRange(poseMatrix, 12, 15), x);
        float distance_norm = matrix.norm(distance_xyz);

        if (distance_norm < thr_max) {

            // If dist < thr_max, there are not large errors in trajectory
//            if ((distance_norm > thr_min) && (init_counter > 10)) {
//            // If dist > thr_min, there is a small error in trajectory -> correct it
//                shift = matrix.add_vectors(shift, distance_xyz);                // update shift
//                poseMatrix = subtract_from_pose(poseMatrix, distance_xyz);      // update pose
//            }
            // Update kalman based on pose

            update_kalman(poseMatrix);
            init_counter++;
        }
        else {
            trajectoryChange = true;
        }

        return poseMatrix;
    }

    private float[] get_prediction() {
        f.predict();
        float[] stateEstimation = matrix.arr_double2float(f.getStateEstimation());
        float[] x = new float[3];
        for (int i = 0; i < stateEstimation.length; i += 3)
            x[i/3] = stateEstimation[i];
        return x;
    }

    private void update_kalman(float[] poseMatrix){
        double[] poseMatrix_xyz = matrix.arr_float2double(Arrays.copyOfRange(poseMatrix,12,15));
        f.correct(poseMatrix_xyz);
    }

    private float[] subtract_from_pose(float[] poseMatrix, float[] subtract_data){
        float[] subPoseMatrix = Arrays.copyOfRange(poseMatrix, 12, 15);
        float[] subtraction = matrix.subtract_vectors(subPoseMatrix, subtract_data);
        System.arraycopy(subtraction, 0, poseMatrix, 12, 3);
        return poseMatrix;
    }

    public boolean isTrajectoryChange(){
        return trajectoryChange;
    }
}