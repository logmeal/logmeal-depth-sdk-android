package es.logmeal.sdk;

import java.util.ArrayList;

public class AnglesAnalysis {

    private static final int MAIN_RGB_ANGLE = 90;
    private static final int MAIN_RGB_ANGLE_ERR = 15; // up to 15 degrees of tolerance

    // First cam proj
    private float[] cam0Proj;

    private int mainRgbIdx;

    // List of frames and additional data
    private ArrayList<Float> angles_list;
    private ArrayList<Integer> anglesIdx_list;
    private ArrayList<float[]> angleRangesList;

    private ArrayList<FrameData> fullFramesList;
    private ArrayList<FrameData> selectedFrames_list;
    private int capturedFramesCount;

    private Matrix matrix = new Matrix();

    public AnglesAnalysis(double angle_start, double angle_end, int capturedFramesMax, float[] initialCam0Proj) {

        // Get angle ranges
        angleRangesList = new ArrayList<>();
        double step = (angle_end - angle_start) / (capturedFramesMax);
        for (int i = 0; i < capturedFramesMax; i++) {
            float[] angleRange = matrix.arr_double2float(new double[]{angle_start + (step * i), angle_start + (step * (i + 1))});
            angleRangesList.add(angleRange);
        }

        // Init data
        mainRgbIdx = -1;
        capturedFramesCount = 0;
        cam0Proj = initialCam0Proj;
        fullFramesList = new ArrayList<>();
    }

    public ArrayList<FrameData> updateFullFramesList(FrameData new_frame) {
        fullFramesList.add(new_frame);
        return fullFramesList;
    }

    public ArrayList<FrameData> selectFrames(float[] center) {

        // Update the angle of each frame according to new center
        updateAngleInformation(center);

        // Count the number of frames that has already been captured (max one per angle range)
        selectedFrames_list = new ArrayList<>();
        ArrayList<Integer> capturedFramesIdx_list = new ArrayList<>();

        // add new angle frames
        for (int i = 0; i < fullFramesList.size(); i++) {
            int angleRange_i = fullFramesList.get(i).getAngleRangeIdx();
            if (!capturedFramesIdx_list.contains(angleRange_i)) {
                capturedFramesIdx_list.add(angleRange_i);
                selectedFrames_list.add(fullFramesList.get(i));
            }
        }

        capturedFramesCount = capturedFramesIdx_list.size();
        selectMainRGB();
        return selectedFrames_list;
    }

    private void selectMainRGB() {

        float minAngleDiff = 1000;
        for (int i = 0; i < selectedFrames_list.size(); i++) {
            float curr_angle = selectedFrames_list.get(i).getAngle();
            float currAngleDiff = Math.abs(curr_angle - MAIN_RGB_ANGLE);
            if (currAngleDiff < minAngleDiff) {
                minAngleDiff = currAngleDiff;
                mainRgbIdx = i;
            }
        }
        if (minAngleDiff > MAIN_RGB_ANGLE_ERR) {
            mainRgbIdx = -1;
        }
    }

    private void updateAngleInformation(float[] center) {
        computeAngles(center);
        computeAnglesIdx();
        for (int i = 0; i < fullFramesList.size(); i++) {
            FrameData frame_i = fullFramesList.get(i);
            frame_i.setAngle(angles_list.get(i));
            frame_i.setAngleRangeIdx(anglesIdx_list.get(i));
            fullFramesList.set(i, frame_i);
        }
    }

    private void computeAngles(float[] center) {

        // Init angles list
        angles_list = new ArrayList<>();

        // Update the height of the first camera pose replacing it by the height of the weighted center.
        // Obtain the position of the first camera with respect to the scene center and normalize
        cam0Proj[1] = center[1];
        float[] cam2CenterVector0 = matrix.normalize(matrix.subtract_vectors(cam0Proj, center));

        for (int i = 0; i < fullFramesList.size(); i++) {
            float[][] cam_i = fullFramesList.get(i).getCameraPoseUpdated2D();

            // Obtain the position of the i camera with respect to the scene center and normalize
            float[] cam2CenterVector_i = matrix.flatten_array(matrix.get_subarray(cam_i, 0, 3, 3, 4));
            cam2CenterVector_i = matrix.normalize(matrix.subtract_vectors(cam2CenterVector_i, center));

            // Obtain the angle between the first and current camera
            float theta = (float) Math.toDegrees(Math.acos(matrix.dotProduct(cam2CenterVector0, cam2CenterVector_i)));
            angles_list.add(i, theta);
        }
    }

    private void computeAnglesIdx() {
        // Init angles idx list
        anglesIdx_list = new ArrayList<>();
        for (int i = 0; i < angles_list.size(); i++) {
            anglesIdx_list.add(computeAngleIdx(angles_list.get(i)));
        }
    }

    private int computeAngleIdx(float angle) {
        for (int i = 0; i < angleRangesList.size(); i++) {
            float range_st = angleRangesList.get(i)[0];
            float range_end = angleRangesList.get(i)[1];
            if ((range_st <= angle) && (angle < range_end)) {
                return i;
            }
        }
        return -1;
    }

    public int getCapturedFramesCount() {
        return capturedFramesCount;
    }

    public int getMainRgbIdx() {
        return mainRgbIdx;
    }

    public void resetFullFrameList() {
        this.fullFramesList = new ArrayList<>();
    }
}
