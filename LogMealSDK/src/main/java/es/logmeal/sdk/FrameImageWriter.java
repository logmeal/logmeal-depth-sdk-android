package es.logmeal.sdk;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.exifinterface.media.ExifInterface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class FrameImageWriter {

    public CompletableFuture<List<String>> generateFilesAsync(List<FrameData> framesList, String folderPath, int mainIdx, String mainImageOutPath) {
        CompletableFuture<List<String>> future = new CompletableFuture<>();

        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        List<CompletableFuture<Void>> futures = new ArrayList<>();

        for (int i = 0; i < framesList.size(); i++) {
            FrameData frameData = framesList.get(i);
            String filename = "image_" + i + ".jpeg";

            int finalI = i;
            CompletableFuture<Void> frameFuture = CompletableFuture.runAsync(() -> {
                File file = new File(folderPath, filename);
                try {
                    boolean result = writeImage(file.getAbsolutePath(), frameData.getByteImage(), frameData.getOriginalSize(), frameData.getMaxSize(), frameData.getOriginalWidth(), frameData.getOriginalHeight());
                    if (result) {
                        frameData.setImagePath(filename);
                    }

                    // check if the frame is the mainIdxFrame
                    if (finalI == mainIdx) {
                        File mainFile = new File(mainImageOutPath);
                        boolean mainImgResult = writeImage(mainImageOutPath, frameData.getByteImage(), frameData.getOriginalSize(), frameData.getMaxSize(), frameData.getOriginalWidth(), frameData.getOriginalHeight());
                        if (mainImgResult && frameData.getOrientation() == Configuration.ORIENTATION_PORTRAIT) {
                            ExifInterface exifInterface = new ExifInterface(mainFile);
                            exifInterface.setAttribute(ExifInterface.TAG_ORIENTATION, String.valueOf(ExifInterface.ORIENTATION_ROTATE_90));
                            exifInterface.saveAttributes();
                        }
                    }
                } catch (IOException e) {
                    future.completeExceptionally(e); // Complete the future exceptionally if an exception occurs
                }
            }, executor);

            futures.add(frameFuture);
        }

        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));

        allOf.thenRun(() -> {
            List<String> filePaths = framesList.stream()
                    .map(FrameData::getImagePath)
                    .collect(Collectors.toList());
            future.complete(filePaths); // Complete the future with the list of file paths
            executor.shutdown();
        });

        return future;
    }

    private boolean writeImage(String filePath, byte[] byteImage, int originalSize, int maxSize, int originalWidth, int originalHeight) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
            Bitmap bitmapImage = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.length);
            float scale = 1;
            if (originalSize > maxSize) {
                scale = (float) maxSize / (float) originalSize;
            }
            bitmapImage = Bitmap.createScaledBitmap(bitmapImage, (int) (scale * originalWidth), (int) (scale * originalHeight), true);
            return bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        }
    }
}
