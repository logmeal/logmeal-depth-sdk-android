package es.logmeal.sdk;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CameraPosesMap extends HashMap<String, float[]> implements Map<String, float[]> {
    public CameraPosesMap(List<float[]> cameraPoses) {
        for (int i = 0; i < cameraPoses.size(); i++) {
            String idx = String.format("%1$" + 4 + "s", i).replace(' ', '0');
            put(idx, cameraPoses.get(i));
        }
    }

    public String toJson() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    public String toPrettyJson() {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return gson.toJson(this);
    }

    @NonNull
    public String toString() {
        return toJson();
    }
}
