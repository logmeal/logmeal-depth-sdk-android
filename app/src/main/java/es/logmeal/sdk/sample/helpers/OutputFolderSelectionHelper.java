package es.logmeal.sdk.sample.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import androidx.annotation.Nullable;

public final class OutputFolderSelectionHelper {
    private static final int REQUEST_CODE_OPEN_DIRECTORY = 1;
    private static final String PREF_NAME = "app_preferences";
    private static final String PREF_OUTPUT_DIRECTORY_URI = "output_directory_uri";

    /** Launch the directory picker to select an output folder. */
    public static void pickDirectory(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        activity.startActivityForResult(intent, REQUEST_CODE_OPEN_DIRECTORY);
    }

    /** Handle the result of the directory picker and persist the selected URI. */
    public static void handleDirectoryResult(int requestCode, int resultCode, @Nullable Intent data, Context context) {
        if (requestCode == REQUEST_CODE_OPEN_DIRECTORY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri directoryUri = data.getData();
                if (directoryUri != null) {
                    context.getContentResolver().takePersistableUriPermission(directoryUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    saveSelectedDirectoryUri(context, directoryUri);
                }
            }
        }
    }

    /** Save the selected directory URI in SharedPreferences. */
    private static void saveSelectedDirectoryUri(Context context, Uri uri) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString(PREF_OUTPUT_DIRECTORY_URI, uri.toString()).apply();
    }

    /** Retrieve the saved directory URI from SharedPreferences. */
    public static Uri getSavedDirectoryUri(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String uriString = prefs.getString(PREF_OUTPUT_DIRECTORY_URI, null);
        return uriString != null ? Uri.parse(uriString) : null;
    }

    /** Check if an output directory has been selected. */
    public static boolean hasSelectedOutputDirectory(Context context) {
        return getSavedDirectoryUri(context) != null;
    }
}
