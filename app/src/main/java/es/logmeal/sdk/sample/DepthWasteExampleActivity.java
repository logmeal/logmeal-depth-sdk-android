package es.logmeal.sdk.sample;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import es.logmeal.sdk.LogMealDepthData;
import es.logmeal.sdk.LogMealRecorder;
import es.logmeal.sdk.sample.helpers.DepthWasteApiHelper;
import es.logmeal.sdk.sample.helpers.ResultsFileHelper;
import es.logmeal.sdk.widgets.DepthRecorderView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class DepthWasteExampleActivity extends AppCompatActivity {
    private static final String TAG = DepthWasteExampleActivity.class.getSimpleName();

    private String token;
    private Uri outputDirectoryUri;

    //region UI elements
    private DepthRecorderView depthRecorderView;
    private Button startRecordingBtn;
    private Button stopRecordingBtn;
    private Button sendResultsBtn;
    private TextView resultsTV;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_depth_waste);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras() != null ? getIntent().getExtras().getString("token") : null;
        outputDirectoryUri = getIntent().getExtras() != null ? Uri.parse(getIntent().getExtras().getString("outFolder")) : null;

        // Initialize LogMealRecorder's view
        depthRecorderView = findViewById(R.id.lmRecorder);
        getLifecycle().addObserver(depthRecorderView);
        depthRecorderView.setAutoStopRecordingEventListener(this::updateUIAfterStopping);
        depthRecorderView.setEnableDebug(true);

        startRecordingBtn = findViewById(R.id.wasteStartRecordingBtn);
        stopRecordingBtn = findViewById(R.id.wasteStopRecordingBtn);
        sendResultsBtn = findViewById(R.id.wasteSendResultsBtn);
        resultsTV = findViewById(R.id.wasteResultsTextView);
        resultsTV.setMovementMethod(new ScrollingMovementMethod());
        EditText apiUrlInput = findViewById(R.id.wasteApiUrlTextInput);

        resultsTV.setOnLongClickListener(v -> {
            copyToClipboard();
            return true;
        });

        apiUrlInput.setText(DepthWasteApiHelper.WASTE_DEPTH_URL);

        if (outputDirectoryUri == null) {
            updateUIErrorStarting(new Exception("out folder not selected"));
        }

        startRecordingBtn.setOnClickListener(view -> startRecording());
        stopRecordingBtn.setOnClickListener(view -> stopRecording());
        sendResultsBtn.setOnClickListener(view -> sendRequest());
    }

    //region UI
    private void startRecording() {
        try {
            if (outputDirectoryUri == null) {
                throw new IOException("out folder not selected");
            }
            depthRecorderView.startRecording();
            updateUIForCapturing();
        } catch (IOException e) {
            Log.e(TAG, "Error creating output directory", e);
            updateUIErrorStarting(e);
        }
    }

    private void stopRecording() {
        depthRecorderView.stopRecording();
        updateUIAfterStopping();
    }

    private void updateUIErrorStarting(Throwable th) {
        String msg = "Error starting recording\n" + th.getMessage();
        runOnUiThread(() -> {
            resultsTV.setText(msg);
            startRecordingBtn.setEnabled(true);
            startRecordingBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            sendResultsBtn.setEnabled(false);
            sendResultsBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            stopRecordingBtn.setEnabled(false);
            stopRecordingBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
        });
    }

    private void updateUIForCapturing() {
        runOnUiThread(() -> {
            resultsTV.setText("Capturing...");
            startRecordingBtn.setEnabled(false);
            startRecordingBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            sendResultsBtn.setEnabled(false);
            sendResultsBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            stopRecordingBtn.setEnabled(true);
            stopRecordingBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        });
    }

    private void updateUIAfterStopping() {
        LogMealRecorder lmRecorder = depthRecorderView.getLogMealRecorder();
        String baseMsg =
                (lmRecorder.isFullCapture() || lmRecorder.isTrajectoryChanged()
                        ? "AUTO-STOPPED capture. "
                        : "Capture stopped by user. ") +
                        (lmRecorder.isTrajectoryChanged() ? "\nCHANGED TRAJECTORY." : "");
        String lastLineMsg = lmRecorder.isSuccessful() ? "\nSUCCESS. Generating depth data..." : "\nERROR. Missing needed info to send to API.";

        String initialMsg = baseMsg + lastLineMsg;
        runOnUiThread(() -> {
            startRecordingBtn.setEnabled(true);
            startRecordingBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));

            stopRecordingBtn.setEnabled(false);
            stopRecordingBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));

            resultsTV.setText(initialMsg);

            // disable send button until depth data is generated
            sendResultsBtn.setEnabled(false);
            sendResultsBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
        });

        if (lmRecorder.isSuccessful()) {
            lmRecorder.getLogMealDepthData()
                    .getFuture()
                    .thenAccept(unused -> {
                        if (lmRecorder.getLogMealDepthData().isSuccessfulGeneration()) {
                            try {
                                ResultsFileHelper.moveResults(getContentResolver(), outputDirectoryUri, lmRecorder.getLogMealDepthData());
                                String successMsg = baseMsg + "\nSUCCESS. Generated depth data. READY.";
                                runOnUiThread(() -> {
                                    resultsTV.setText(successMsg);
                                    sendResultsBtn.setEnabled(true);
                                    sendResultsBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                                });
                            } catch (IOException e) {
                                Log.e(TAG, "Error moving files", e);
                                updateUIErrorStarting(e);
                            }
                        }
                    }).exceptionally(throwable -> {
                        Log.e(TAG, "Error generating depth data", throwable);
                        updateUIErrorStarting(throwable);
                        return null;
                    });
        }
    }

    private void copyToClipboard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("LM Depth copied", resultsTV.getText().toString());
        clipboard.setPrimaryClip(clip);
        // Show a Toast message (optional)
        Toast.makeText(this, "Result copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    public void sendRequest() {
        runOnUiThread(() -> {
            sendResultsBtn.setEnabled(false);
            sendResultsBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.fancy_dark_gray));
            resultsTV.setText("Sending...");
        });

        TextInputEditText apiUrlTextInput = findViewById(R.id.wasteApiUrlTextInput);
        String customUrl = Objects.requireNonNull(apiUrlTextInput.getText()).toString();
        TextInputEditText preconcumptionIdTextInput = findViewById(R.id.preconsumptionImageIdTextInput);
        String preconcumptionId = Objects.requireNonNull(preconcumptionIdTextInput.getText()).toString();
        String authToken = this.token != null ? this.token : "";

        DepthWasteApiHelper apiSender = new DepthWasteApiHelper();
        apiSender.setCustomUrl(customUrl);

        LogMealDepthData depthData = depthRecorderView.getLogMealRecorder().getLogMealDepthData();

        Call call = apiSender.sendRequest(authToken,
                preconcumptionId,
                depthData.getMainRgbPath(),
                depthData.getVideoPath(),
                depthData.getCamFocalLength().toJson(),
                depthData.getCamPrincipalPoint().toJson(),
                String.valueOf(depthData.getMainRgbIdx()),
                depthData.getCameraPoses().toJson(),
                depthData.getVersion());

        call.enqueue(new Callback() {
            final String separator = "\r\n========================\r\n";

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                String resultText;
                if (response.isSuccessful()) {
                    int statusCode = response.code();
                    if (statusCode < 200 || statusCode >= 300) {
                        resultText = "ERROR: API respond with error " + separator + "Status Code: " + statusCode + separator + response.body().string();
                    } else {
                        resultText = "SUCCESS " + separator + "Status Code: " + statusCode + separator;
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            Log.d("Response JSON", jsonObject.toString());
                            resultText += "ImageID: " + jsonObject.optString("imageId", "-") + separator + jsonObject;
                        } catch (JSONException e) {
                            resultText += "Error parsing response";
                            Log.e(TAG, "Error parsing response", e);
                        }
                    }
                } else {
                    resultText = "ERROR: Could not send request " + separator + response.message() + separator + response.body().string();
                    Log.d(TAG, String.format("Error sending request: %s - %s", response.message(), response.body().string()));
                }

                String finalResultText = resultText;
                runOnUiThread(() -> {
                    resultsTV.setText(finalResultText);
                    sendResultsBtn.setEnabled(true);
                    sendResultsBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                });
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "Error sending request", e);
                runOnUiThread(() -> {
                    resultsTV.setText("ERROR: Could not send request " + separator + e.getMessage());
                    sendResultsBtn.setEnabled(true);
                    sendResultsBtn.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                });
            }
        });
    }
    //endregion
}
