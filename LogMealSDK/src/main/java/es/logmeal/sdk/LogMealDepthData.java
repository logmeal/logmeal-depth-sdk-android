/*
 * This class is created to manage the input for the API of LogMeal
 * */
package es.logmeal.sdk;

import android.util.Log;

import com.arthenica.ffmpegkit.FFmpegKit;
import com.arthenica.ffmpegkit.FFmpegSession;
import com.arthenica.ffmpegkit.ReturnCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class LogMealDepthData {
    private static final String TAG = LogMealDepthData.class.getSimpleName();

    private final String outputFolder;
    private String debugFilePath;
    private CameraPosesMap cameraPoses;
    private CamPoint camFocalLength;
    private CamPoint camPrincipalPoint;
    private int mainRgbIdx;
    private final String mainRgbPath;
    private final String videoPath;
    private final String version = "android";
    private boolean enableDebug = false;
    private LogMealDepthDataState logMealDepthDataState;
    private CompletableFuture<Void> future;

    public LogMealDepthData(String outPath) {
        logMealDepthDataState = LogMealDepthDataState.IDLE;
        outputFolder = outPath;
        videoPath = Paths.get(outPath, "output.mp4").toString();
        mainRgbPath = Paths.get(outPath, "main_img_rgb.jpg").toString();
    }

    public LogMealDepthData(String outPath, boolean enableDebug) {
        logMealDepthDataState = LogMealDepthDataState.IDLE;
        outputFolder = outPath;
        videoPath = Paths.get(outPath, "output.mp4").toString();
        mainRgbPath = Paths.get(outPath, "main_img_rgb.jpg").toString();
        this.enableDebug = enableDebug;
    }

    public CompletableFuture<Void> generate(List<FrameData> framesList, int mainRgbIdx) {

        if (framesList.isEmpty()) {
            return CompletableFuture.runAsync(() -> {
                throw new IllegalArgumentException("Frames list is empty");
            });
        }

        long start = System.currentTimeMillis();
        logMealDepthDataState = LogMealDepthDataState.IS_GENERATING;
        camFocalLength = new CamPoint(framesList.get(0).getFocalLength()[0], framesList.get(0).getFocalLength()[1]);
        camPrincipalPoint = new CamPoint(framesList.get(0).getPrincipalPoint()[0], framesList.get(0).getPrincipalPoint()[1]);
        cameraPoses = new CameraPosesMap(framesList.stream().map(FrameData::getCameraPoseUpdated).collect(Collectors.toList()));
        this.mainRgbIdx = mainRgbIdx;
        long startImg = System.currentTimeMillis();

        future = new FrameImageWriter()
                .generateFilesAsync(framesList, outputFolder, mainRgbIdx, mainRgbPath)
                .thenCompose(imgPaths -> {
                    Log.d(TAG, "IMAGES GENERATED IN " + (System.currentTimeMillis() - startImg) + "ms");
                    long startVid = System.currentTimeMillis();
                    return generateVideo(imgPaths).thenCompose(aBoolean -> {
                                Log.d(TAG, "VIDEO GENERATED IN " + (System.currentTimeMillis() - startVid) + "ms");
                                Log.d(TAG, "TOTAL TIME IN " + (System.currentTimeMillis() - start) + "ms");

                                return generateJSONDebugFile(framesList).thenAccept(unused -> {
                                            logMealDepthDataState = LogMealDepthDataState.DONE_GENERATING;
                                        })
                                        .exceptionally(e -> {
                                            logMealDepthDataState = LogMealDepthDataState.ERROR;
                                            return null;
                                        });
                            })
                            .exceptionally(e -> {
                                logMealDepthDataState = LogMealDepthDataState.ERROR;
                                return null;
                            });
                }).exceptionally(e -> {
                    logMealDepthDataState = LogMealDepthDataState.ERROR;
                    return null;
                });

        return future;
    }


    /**
     * Method that writes in memory the JSON. The JSON data needed are: all the camera poses of the
     * frames taken, the focal length and principal point of the camera, the index of the main
     * RGB image and the version of the operating system
     *
     * @param frameList
     */
    private CompletableFuture<Void> generateJSONDebugFile(List<FrameData> frameList) {

        if (!enableDebug) {
            return CompletableFuture.completedFuture(null);
        }

        debugFilePath = Paths.get(outputFolder, "apiinput.json").toString();

        return CompletableFuture.supplyAsync(() -> {

            try {
                String cameraPose = "{";
                for (int i = 0; i < frameList.size(); i++) {
                    String idx = String.format("%1$" + 4 + "s", i).replace(' ', '0');
                    cameraPose += "\"" + idx + "\":" + Arrays.toString(frameList.get(i).getCameraPoseUpdated());
                    if (i < frameList.size() - 1) {
                        cameraPose += ",";
                    }
                }
                cameraPose += "}";

                String cameraPoseOrg = "{";
                for (int i = 0; i < frameList.size(); i++) {
                    String idx = String.format("%1$" + 4 + "s", i).replace(' ', '0');
                    cameraPoseOrg += "\"" + idx + "\":" + Arrays.toString(frameList.get(i).getCameraPose());
                    if (i < frameList.size() - 1) {
                        cameraPoseOrg += ",";
                    }
                }
                cameraPoseOrg += "}";

                String camera_angle = "{";
                for (int i = 0; i < frameList.size(); i++) {
                    String idx = String.format("%1$" + 4 + "s", i).replace(' ', '0');
                    camera_angle += "\"" + idx + "\":" + frameList.get(i).getAngle();
                    if (i < frameList.size() - 1) {
                        camera_angle += ",";
                    }
                }
                camera_angle += "}";

                String info = "{" +
                        "\"version\":" + version + "," +
                        "\"main_rgb_idx\":" + mainRgbIdx + "," +
                        "\"cam_focal_length\":" + camFocalLength.toJson() + "," +
                        "\"cam_principal_point\":" + camPrincipalPoint.toJson() + "," +
                        "\"camera_pose\":" + cameraPose + "," +
                        "\"camera_pose_org\":" + cameraPoseOrg + "," +
                        "\"camera_angle\":" + camera_angle +
                        "}";

                JSONObject jsonObject = new JSONObject(info);
                File infoFile = new File(debugFilePath);
                BufferedWriter output = new BufferedWriter(new FileWriter(infoFile));
                output.write(jsonObject.toString());
                output.close();
            } catch (IOException | JSONException ex) {
                Log.e(TAG, "Error generating JSON debug file", ex);
                throw new RuntimeException(ex);
            }
            return null;
        });
    }

    private CompletableFuture<Void> generateVideo(List<String> imgFileNames) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        executor.execute(() -> {
            try {
                String imagesPaths = outputFolder + File.separator + "images.txt";

                // write file paths to single txt file
                try (FileWriter fw = new FileWriter(imagesPaths)) {
                    for (String imgPath : imgFileNames) {
                        fw.write("file '" + imgPath + "'\n");
                    }
                }

                String command = "-f concat -i " + imagesPaths +
                        " -c:v mpeg4 -pix_fmt yuv420p -framerate 50 -b:v 2000k -y " +
                        videoPath;

                FFmpegSession session = FFmpegKit.execute(command);
                if (ReturnCode.isSuccess(session.getReturnCode())) {
                    Log.d(TAG, "VIDEO SUCCESSFULLY GENERATED");
                    future.complete(null); // Complete the future when the operation is done
                } else if (ReturnCode.isCancel(session.getReturnCode())) {
                    Log.d(TAG, "VIDEO CANCELED");
                    future.completeExceptionally(new Exception("VIDEO CANCELED"));
                } else {
                    String errMsg = String.format("Command failed with state %s and rc %s.%s", session.getState(), session.getReturnCode(), session.getFailStackTrace());
                    Log.d(TAG, errMsg);
                    future.completeExceptionally(new Exception(errMsg));
                }
            } catch (IOException e) {
                future.completeExceptionally(e); // Complete the future exceptionally if an exception occurs
            } finally {
                executor.shutdown(); // Shut down the executor
            }
        });

        return future;
    }

    public void clearTempFiles(List<String> imgNamesList) {

        if (enableDebug) {
            return;
        }

        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        List<CompletableFuture<Void>> futures = new ArrayList<>();

        for (String imgName : imgNamesList) {
            CompletableFuture<Void> deleteFuture = CompletableFuture.runAsync(() -> {
                File file = new File(outputFolder, imgName);
                if (file.exists() && file.delete()) {
                    future.complete(null);
                } else {
                    future.completeExceptionally(new Exception("No file to delete"));
                }
            }, executor);
            futures.add(deleteFuture);
        }

        CompletableFuture<Void> deleteFuture = CompletableFuture.runAsync(() -> {
            File file = new File(outputFolder, "images.txt");
            if (file.exists() && file.delete()) {
                future.complete(null);
            } else {
                future.completeExceptionally(new Exception("No file to delete"));
            }
        }, executor);
        futures.add(deleteFuture);


        CompletableFuture<Void> allOf = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        allOf.thenRun(() -> {
            future.complete(null); // Complete the future with the list of file paths
            executor.shutdown();
        });
    }

    public String getMainRgbPath() {
        return mainRgbPath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public CamPoint getCamFocalLength() {
        return camFocalLength;
    }

    public CamPoint getCamPrincipalPoint() {
        return camPrincipalPoint;
    }

    public CameraPosesMap getCameraPoses() {
        return cameraPoses;
    }

    public int getMainRgbIdx() {
        return mainRgbIdx;
    }

    public String getVersion() {
        return version;
    }

    public LogMealDepthDataState getLogMealDepthDataState() {
        return logMealDepthDataState;
    }

    public boolean isSuccessfulGeneration() {
        return logMealDepthDataState == LogMealDepthDataState.DONE_GENERATING;
    }

    public CompletableFuture<Void> getFuture() {
        return future;
    }

    public String getDebugFilePath() {
        return debugFilePath;
    }
}
