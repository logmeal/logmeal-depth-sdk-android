# 4.0.2

## Bug fixes

- Remove depth from uses-feature

# 4.0.1

## Bug fixes
- [FIX] Prevent anchor native pose error on cold starts

## Other
- Refactor and remove unused and deprecated APIs
- [IMPROVE] Now the SDK works with ffmpeg-kit-min, reducing size by 30mb approximately

# 4.0.0

## Breaking changes

- Changed signature: `ArCoreUtils.isDeviceSupported(AppCompatActivity)` -> `ArCoreUtils.isDeviceSupported(Activity);`. This change was made to simplify integration with Kotlin and Jetpack Compose.

## Features

- Improve documentation (section 1)

## Bug fixes

- Correctly declare imports in generated AAR file
- Correctly declare hardware features and API
