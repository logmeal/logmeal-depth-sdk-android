package es.logmeal.sdk.sample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.Objects;

import es.logmeal.sdk.helpers.ArCoreSupport;
import es.logmeal.sdk.helpers.ArCoreUtils;
import es.logmeal.sdk.helpers.CameraPermissionHelper;
import es.logmeal.sdk.sample.helpers.OutputFolderSelectionHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openWasteDepthExample(View view) {
        Intent intent = new Intent(this, DepthWasteExampleActivity.class);
        intent.putExtra("token", ((EditText) findViewById(R.id.tokenInput)).getText().toString());
        intent.putExtra("outFolder", Objects.requireNonNull(OutputFolderSelectionHelper.getSavedDirectoryUri(this)).toString());
        startActivity(intent);
    }

    public void openDepthImageExample(View view) {
        Intent intent = new Intent(this, DepthImageExampleActivity.class);
        intent.putExtra("token", ((EditText) findViewById(R.id.tokenInput)).getText().toString());
        intent.putExtra("outFolder", Objects.requireNonNull(OutputFolderSelectionHelper.getSavedDirectoryUri(this)).toString());
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // ARCore requires camera permission to operate.
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            CameraPermissionHelper.requestCameraPermission(this);
        }

        checkDeviceSupport();
    }

    void checkDeviceSupport() {
        ArCoreSupport arCoreSupport = ArCoreUtils.isDeviceSupported(this);

        TextView textView = findViewById(R.id.textViewDepthSupport);
        Button installApkButton = findViewById(R.id.button_install_arcore);
        Button selectFolderButton = findViewById(R.id.select_folder_btn);

        if (arCoreSupport.isArCoreSupported()) {
            if (arCoreSupport.isDepthSupported()) {
                runOnUiThread(() -> {
                    textView.setText(getString(R.string.depth_supported_text));
                    textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.YellowGreen));
                });
            } else {
                runOnUiThread(() -> {
                    textView.setText(getString(R.string.depth_not_supported_text));
                    textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.MediumVioletRed));
                });
            }

            runOnUiThread(() -> {
                installApkButton.setVisibility(View.INVISIBLE);
                selectFolderButton.setVisibility(View.VISIBLE);
            });
            checkOutFolderSelected();
        } else if (arCoreSupport.getArCoreAvailability().isSupported() || arCoreSupport.getArCoreAvailability().isUnknown()) {
            runOnUiThread(() -> {
                textView.setText(getString(R.string.arcore_support_unknown));
                textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.AliceBlue));
                installApkButton.setVisibility(View.VISIBLE);
            });
        } else {
            runOnUiThread(() -> {
                textView.setText(getString(R.string.arcore_not_supported));
                textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.MediumVioletRed));
            });
        }
    }

    private void checkOutFolderSelected() {
        if (OutputFolderSelectionHelper.hasSelectedOutputDirectory(this)) {
            Button openWidgetExampleBtn = findViewById(R.id.button_open_widget_example);
            Button openAdvancedExampleBtn = findViewById(R.id.button_open_advanced_example);
            EditText tokenInput = findViewById(R.id.tokenInput);
            TextView textView = findViewById(R.id.textViewDepthSupport);
            String newText = textView.getText() + "\n SELECTED FOLDER = " + OutputFolderSelectionHelper.getSavedDirectoryUri(this);
            runOnUiThread(() -> {
                textView.setText(newText);
                openWidgetExampleBtn.setVisibility(View.VISIBLE);
                openAdvancedExampleBtn.setVisibility(View.VISIBLE);
                tokenInput.setVisibility(View.VISIBLE);
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] results) {
        super.onRequestPermissionsResult(requestCode, permissions, results);
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                    .show();
            if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                CameraPermissionHelper.launchPermissionSettings(this);
            }
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        OutputFolderSelectionHelper.handleDirectoryResult(requestCode, resultCode, data, this);
    }

    public void pickDirectory(View view) {
        OutputFolderSelectionHelper.pickDirectory(this);
    }
}