package es.logmeal.sdk.widgets;

public interface DepthRecorderAutoStopListener {
    void onAutoStopRecording();
}
